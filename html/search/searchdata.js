var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxyz",
  1: "abcdefklmnpr",
  2: "cdgmpstu",
  3: "abcdefgikmnpqrstuvwxz",
  4: "abcdfghiklmnoprstuvwxy",
  5: "cmp",
  6: "cnsw",
  7: "bchilmstw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerator",
  7: "Macros"
};

