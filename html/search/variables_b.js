var searchData=
[
  ['name',['name',['../dwm_8c.html#a9121c70de86ef72f674bc8f0ce2824d0',1,'Client']]],
  ['netatom',['netatom',['../dwm_8c.html#a117248a1ae9e17a933c48e3be72b2d07',1,'dwm.c']]],
  ['neverfocus',['neverfocus',['../dwm_8c.html#a17e467d58ff8cbeaf7e5abd2ae6e76fc',1,'Client']]],
  ['next',['next',['../dwm_8c.html#a0d3af97d0d88d154b808f58265660b69',1,'Client::next()'],['../dwm_8c.html#a1359c52c4f322d7478a9c8f67eb01d91',1,'Monitor::next()']]],
  ['nmaster',['nmaster',['../dwm_8c.html#ad22c95f55906980326a43c09739267ef',1,'Monitor::nmaster()'],['../config_8def_8h.html#a26effc5d433ad34035a04e2f71d7f811',1,'nmaster():&#160;config.def.h'],['../config_8h.html#a26effc5d433ad34035a04e2f71d7f811',1,'nmaster():&#160;config.h']]],
  ['nmasters',['nmasters',['../dwm_8c.html#ac25d4a7e73083da01e62793d8ed06576',1,'Pertag']]],
  ['normbgcolor',['normbgcolor',['../config_8def_8h.html#a1580a91259af4ffd5c28d24f24dbd033',1,'normbgcolor():&#160;config.def.h'],['../config_8h.html#a1580a91259af4ffd5c28d24f24dbd033',1,'normbgcolor():&#160;config.h']]],
  ['normbordercolor',['normbordercolor',['../config_8def_8h.html#a5721ce382ca6d2d945a43aac2dde66d4',1,'normbordercolor():&#160;config.def.h'],['../config_8h.html#a5721ce382ca6d2d945a43aac2dde66d4',1,'normbordercolor():&#160;config.h']]],
  ['normfgcolor',['normfgcolor',['../config_8def_8h.html#a9b18effb69a9ea40f2c1ad6a9cc4a72f',1,'normfgcolor():&#160;config.def.h'],['../config_8h.html#a9b18effb69a9ea40f2c1ad6a9cc4a72f',1,'normfgcolor():&#160;config.h']]],
  ['num',['num',['../dwm_8c.html#a8c2521f08657ccb776ecb59f7c4edd55',1,'Monitor']]],
  ['numlockmask',['numlockmask',['../dwm_8c.html#a548cb6e9bda721b085c94748b1eb9fc3',1,'dwm.c']]]
];
